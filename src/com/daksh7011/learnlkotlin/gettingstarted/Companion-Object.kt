/*
 * Cooked in kitchen of Daksh (SlothDemon)
 */

package com.daksh7011.learnlkotlin.gettingstarted

class CompanionObject {
    companion object {
        /*  Annotation @JvnStatic defines the main method static just like classic core java has
            public static void main (String args) {
            }
        */
        @JvmStatic
        fun main(args: Array<String>) {
            println("Hello with Companion Object")
        }
    }
}