/*
 * Cooked in kitchen of Daksh (SlothDemon)
 */

package com.daksh7011.learnlkotlin.gettingstarted

fun main(args: Array<String>) {
    println("Please enter value for A and B")
    // Use val instead of var when you are not modifying the value of variable
    val (a, b) = readLine()!!.split(" ") //!! is used for not null asserted calls that prevents NPEs
    println("Max number is : ${maxNumber(a.toInt(), b.toInt())}") //method call
    /*Dissecting the method call.
    * we are printing the string Max number is and then calling method with $
    */
}

fun maxNumber(a: Int, b: Int): Int {
    //store value of a in max if a is greater than b, if not vice-versa
    val max = if (a>b){
        println("The value of a is $a") // $a will print the value of a. Isn't that beautiful?
        a //stores value of a in max if condition is true
    }
    else{
        println("The value of b is $b")
        b //stores value of b in max when condition is false
    }
    //then return max
    return max

    /*The block above can be replaced by this simple hack. It saves memory as we are not using variable max anymore
    return if (a>b){
        println("The value of a is $a")
        a
    }
    else{
        println("The value of b is $b")
        b
    }*/

}