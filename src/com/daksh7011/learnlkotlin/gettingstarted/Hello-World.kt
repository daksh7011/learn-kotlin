/*
 * Cooked in kitchen of Daksh (SlothDemon)
 */

package com.daksh7011.learnlkotlin.gettingstarted

// kotlin conversion of main function in java with no class specified
fun main(args: Array<String>) {
    println("Hello from the other side!")
}