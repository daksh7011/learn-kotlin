/*
 * Cooked in kitchen of Daksh (SlothDemon)
 */

package com.daksh7011.learnlkotlin.gettingstarted

class CompanionObject2 {
    companion object {
        // Slightly different than Companion-Object.kt
        @JvmStatic
        fun main(args: Array<String>) {
            //calling run method
            CompanionObject2.run()
        }
        fun run(){
            println("Hello World with run function")
        }
    }
}